import pytest
from selene import browser, be, have



def test_google_searching():
    browser.open('https://google.com')
    browser.element('[name="q"]').should(be.blank).type('yashaka/selene').press_enter()
    browser.element('[id="search"]').should(have.text('Selene - User-oriented Web UI browser tests in Python'))

def test_google_searching_empty_result():
    browser.open("https://google.ru")
    browser.element('[name="q"]').should(be.blank).type('uqioozonasdeefggg!!2fdasffgsafa').press_enter()
    browser.element('#botstuff').should(have.text('По запросу uqioozonasdeefggg!!2fdasffgsafa ничего не найдено.'))