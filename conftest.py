import pytest
from selene import browser

@pytest.fixture(autouse=True)
def browser_size():
    browser.config.window_height = 1920 # задает высоту окна браузера
    browser.config.window_width = 1080  # задает ширину окна браузера
    yield
    browser.quit()